insert into State(name, uf) values('Pará', 'PA');
insert into State(name, uf) values('Amazonas', 'AM');
insert into State(name, uf) values('Rondônia', 'RO');
insert into State(name, uf) values('Acre', 'AC');
insert into State(name, uf) values('Roraima', 'RR');

insert into City(name, state_id ) values('Belém', 1);
insert into City(name, state_id ) values('Ananindeua', 1);
insert into City(name, state_id ) values('Castanhal', 1);
insert into City(name, state_id ) values('Manaus', 2);
insert into City(name, state_id ) values('Parintins', 2);
insert into City(name, state_id ) values('Porto Velho', 3);
insert into City(name, state_id ) values('Rio Branco', 4);
insert into City(name, state_id ) values('Boa Vista', 5);